﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionDB
{
    public static void Init()
    {
        foreach(var kvp in Conditions)
        {
            var conditionId = kvp.Key;
            var condition = kvp.Value;

            condition.Id = conditionId;
        }
    }

    public static Dictionary<ConditionID, Condition> Conditions { get; set; } = new Dictionary<ConditionID, Condition>()
    {
        {
            ConditionID.saignement,
            new Condition()
            {
                Name = "Bleed",
                StartMessage = "is bleeding",
                OnAfterTurn = (Chimere chimere) =>
                {
                    chimere.UpdateHP(chimere.MaxHp / 8);
                    chimere.StatusChanges.Enqueue($"{chimere.Base.Name} is bleeding");
                }
            }
        },
        {
            //une chimere blessé ne peut plus utiliser certaines attaques
            ConditionID.blessure,
            new Condition()
            {
                Name = "Hurt",
                StartMessage = "has been hurt",
                OnBeforeMove = (Chimere chimere) =>
                {
                    if(Random.Range(1, 5) == 1)
                    {
                        chimere.CureStatus();
                        chimere.StatusChanges.Enqueue($"{chimere.Base.Name} is not hurt anymore");
                        return true;
                    }
                    return true;
                }
            }
        },
        {
            ConditionID.assome,
            new Condition()
            {
                Name = "Sleep",
                StartMessage = "fallen asleep",
                OnStart = (Chimere chimere) =>
                {
                    //sleep for 3 turns
                    chimere.StatusTime = 1;
                    Debug.Log($"Will be asleep for {chimere.StatusTime} moves");
                },
                OnBeforeMove = (Chimere chimere) =>
                {
                    if (chimere.StatusTime <= 0)
                    {
                        chimere.CureStatus();
                        chimere.StatusChanges.Enqueue($"{chimere.Base.Name} wope up!");
                        return true;
                    }

                    chimere.StatusTime--;
                    chimere.StatusChanges.Enqueue($"{chimere.Base.Name} is sleeping");
                    return false;
                }
            }
        },
        // Volatile Status Condition
        {
            ConditionID.confusion,
            new Condition()
            {
                Name = "Confusion",
                StartMessage = "has been confused ",
                OnStart = (Chimere chimere) =>
                {
                    //confuse for 1-4 turns
                    chimere.VolatileStatusTime = Random.Range(1,5);
                    Debug.Log($"Will be confuse for {chimere.StatusTime} moves");
                },
                OnBeforeMove = (Chimere chimere) =>
                {
                    if (chimere.StatusTime <= 0)
                    {
                        chimere.CureVolatileStatus();
                        chimere.StatusChanges.Enqueue($"{chimere.Base.Name} kicked out for confusion !");
                        return true;
                    }
                    chimere.StatusTime--;
                    // 50% chance to do a move
                    if(Random.Range(1,3) == 1)
                        return true;

                    //Hurt by confusing
                    chimere.StatusChanges.Enqueue($"{chimere.Base.Name} is confused");
                    chimere.UpdateHP(chimere.MaxHp /8);
                    chimere.StatusChanges.Enqueue($"It heurt itself due to confusion");
                    return false;
                }
            }
        },
    };
}
public enum ConditionID
{
    none,saignement,blessure,piege,assome,
    confusion
}
