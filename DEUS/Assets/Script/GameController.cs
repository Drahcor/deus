﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { FreeRoam,Battle}

public class GameController : MonoBehaviour {

    GameState state;

    [SerializeField] PlayerManager playerManager;
    [SerializeField] BattleSystem battleSystem;
    [SerializeField] Camera worldCamera;


    private void Awake()
    {
        ConditionDB.Init();
    }
    public void Start()
    {
        playerManager.OnEncounter += StartBattle;
        battleSystem.OnBattleOver += EndBattle;
    }

    void StartBattle()
    {
         state = GameState.Battle;
         battleSystem.gameObject.SetActive(true);
         worldCamera.gameObject.SetActive(false);

        var playerParty = playerManager.GetComponent<ChimereParty>();
        var wildChimereRandom = FindObjectOfType<MapArea>().GetComponent<MapArea>().GetRandomWildChimere();

        battleSystem.StartBattle(playerParty, wildChimereRandom);
    }
    void EndBattle(bool won)
    {
        state = GameState.FreeRoam;
        battleSystem.gameObject.SetActive(false);
        worldCamera.gameObject.SetActive(true);
    }

    private void Update()
    {
        if(state == GameState.FreeRoam)
        {
            playerManager.HandleUpdate();
        }
        else if (state == GameState.Battle)
        {
            battleSystem.HandleUpdate();
        }
    }
}
