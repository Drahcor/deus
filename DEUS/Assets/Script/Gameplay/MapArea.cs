﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapArea : MonoBehaviour
{
    [SerializeField] List<Chimere> wildChimeresRandom;
    [SerializeField] Chimere wildChimeres;

    [SerializeField] bool isRandom;

    public Chimere GetRandomWildChimere()
    {
        var wildChimereRandom = wildChimeresRandom[Random.Range(0, wildChimeresRandom.Count)];
        wildChimereRandom.Init();

        if (isRandom)
        {
            return wildChimereRandom;
        }
        else
        {
            var wildChimere = wildChimeres;
            wildChimere.Init();
            return wildChimere;
        }
    }
}
