﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUnit : MonoBehaviour
{
    BattleSystem battleSystem;

    public GameObject chimere3D;
    public GameObject cloneChimere3D; // vérifier si il peut etre retirer

    public Transform playerTransform;
    public Transform ennemyTransform;

    public Chimere Chimere { get; set; }

    [SerializeField] bool isPlayerUnit;
    [SerializeField] BattleHud hud;

    public BattleHud Hud
    {
        get { return hud; }
    }

    public bool IsPlayerUnit
    {
        get { return IsPlayerUnit; }
    }

    public void Setup(Chimere chimere)
    {
        Chimere = chimere;
        chimere3D = Chimere.Base.Chimere3D.gameObject;

        hud.SetData(chimere);

        if (isPlayerUnit)
        {
            if(cloneChimere3D == null)
            {
                // instancier chimere player
                cloneChimere3D = Instantiate(chimere3D, playerTransform.transform.position, Quaternion.identity);
            }
            else
            {
                Destroy(cloneChimere3D);
                cloneChimere3D = Instantiate(chimere3D, playerTransform.transform.position, Quaternion.identity);
            }
        }
        else
        {
            // instancier chimere ennemi
            if (cloneChimere3D == null)
            {
                cloneChimere3D = Instantiate(chimere3D, ennemyTransform.transform.position, Quaternion.identity);
            }
            else
            {
                Destroy(cloneChimere3D);
                cloneChimere3D = Instantiate(chimere3D, ennemyTransform.transform.position, Quaternion.identity);
            }
        }
    }
    public void PlayEnterAnimation()
    {

    }
}