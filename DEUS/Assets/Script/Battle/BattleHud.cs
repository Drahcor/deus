﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHud : MonoBehaviour
{
    [SerializeField] Text nameText;
    [SerializeField] Text levelText;
    [SerializeField] Text statusText;
    [SerializeField] HPBar hpBar;

    [SerializeField] Color bleedColor;
    [SerializeField] Color hurtColor;
    [SerializeField] Color sleepColor;
    [SerializeField] Color confuseColor;

    Chimere _chimere;
    Dictionary<ConditionID, Color> statusColor;

    public void SetData(Chimere chimere)
    {
        _chimere = chimere;

        nameText.text = chimere.Base.Name;
        levelText.text = "Lvl " + chimere.Level;
        hpBar.SetHP((float)chimere.HP / chimere.MaxHp);

        statusColor = new Dictionary<ConditionID, Color>()
        {
            {ConditionID.saignement, bleedColor },
            {ConditionID.blessure, hurtColor },
            {ConditionID.assome, sleepColor },
            {ConditionID.confusion, confuseColor}
        };

        SetStatusText();
        _chimere.OnStatusChanged += SetStatusText;
    }
    void SetStatusText()
    {
        if(_chimere.Status == null)
        {
            statusText.text = "";
        }
        else
        {
            statusText.text = _chimere.Status.Id.ToString().ToUpper();
            statusText.color = statusColor[_chimere.Status.Id];
        }
    }

    public IEnumerator UpdateHP()
    {
        if (_chimere.HpChanged)
        {
            yield return hpBar.SetHPSmooth((float)_chimere.HP / _chimere.MaxHp);
            _chimere.HpChanged = false;
        }
    }
}
