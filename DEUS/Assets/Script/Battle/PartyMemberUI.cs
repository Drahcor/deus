﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyMemberUI : MonoBehaviour
{
    [SerializeField] Text nameText;
    [SerializeField] Text levelText;
    [SerializeField] HPBar hpBar;

    [SerializeField] Color highlightedColor; // change de couleur le membre choisie pour switch de pokemon en combat

    Chimere _chimere;

    public void SetData(Chimere chimere)
    {
        _chimere = chimere;

        nameText.text = chimere.Base.Name;
        levelText.text = "Lvl" + chimere.Level;
        hpBar.SetHP((float)chimere.HP / chimere.MaxHp);
    }

    public void SetSelected(bool selected)
    {
        if (selected)
            nameText.color = highlightedColor;
        else
            nameText.color = Color.black;
    }
}
