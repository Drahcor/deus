﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyScreen : MonoBehaviour
{
    [SerializeField ]Text messageText;

    PartyMemberUI[] membersSlots;
    List<Chimere> chimeres;

    public void Init()
    {
        membersSlots = GetComponentsInChildren<PartyMemberUI>();
    }

    public void SetPartyDate(List<Chimere> chimeres)
    {
        this.chimeres = chimeres;

        for(int i = 0; i < membersSlots.Length; i++)
        {
            if (i < chimeres.Count)
                membersSlots[i].SetData(chimeres[i]);
            else
                membersSlots[i].gameObject.SetActive(false);
        }

        messageText.text = "Choose a chimere";
    }

    public void UpdateMemberSelection(int selectedMember)
    {
        for(int i = 0; i < chimeres.Count; i++)
        {
            if (i == selectedMember)
                membersSlots[i].SetSelected(true);
            else
                membersSlots[i].SetSelected(false);
        }
    }

    public void SetMessageText(string message)
    {
        messageText.text = message;
    }
}
