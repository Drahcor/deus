﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public enum BattleState { Start, ActionSelection, MoveSelection, RunningTurn, Busy, PartyScreen, BattleOver}
public enum BattleAction { Move,SwitchChimere, UseItem, Run}

public class BattleSystem : MonoBehaviour
{
    [SerializeField] BattleUnit playerUnit;
    [SerializeField] BattleUnit ennemyUnit;
    [SerializeField] BattleDialogBox dialogBox;
    [SerializeField] PartyScreen partyScreen;

    public event Action<bool> OnBattleOver;

    BattleState state;
    BattleState? prevStat; //retourner en arrière
    int currentAction;
    int currentMove;
    int currentMember;

    ChimereParty playerParty;
    Chimere wildChimere;

    public void StartBattle(ChimereParty playerParty, Chimere wildChimere)
    {
        this.playerParty = playerParty;
        this.wildChimere = wildChimere;
        StartCoroutine(SetupBattle());
    }

    public IEnumerator SetupBattle()
    {
        playerUnit.Setup(playerParty.GetHealthyChimere());
        ennemyUnit.Setup(wildChimere);

        partyScreen.Init();

        dialogBox.SetMoveNames(playerUnit.Chimere.Moves);

        // dialogue du début de combat
       yield return dialogBox.TypeDialog($"A wild chimere {ennemyUnit.Chimere.Base.Name} appeare");
       yield return new WaitForSeconds(1f);

        ActionsSelection();
    }
    void BattleOver(bool won)
    {
        state = BattleState.BattleOver;
        playerParty.Chimeres.ForEach(p => p.OnBattleOver());
        OnBattleOver(won);
    }
    void ActionsSelection()
    {
        state = BattleState.ActionSelection;
        dialogBox.SetDialog("Choose an action");
        dialogBox.EnableActionSelector(true);
    }
    void OpenPartyScreen()
    {
        state = BattleState.PartyScreen;
        partyScreen.SetPartyDate(playerParty.Chimeres);
        partyScreen.gameObject.SetActive(true);
    }
    void MoveSelection()
    {
        state = BattleState.MoveSelection;
        dialogBox.EnableActionSelector(false);
        dialogBox.EnableDialogText(false);
        dialogBox.EnableMoveSelector(true);
    }

    IEnumerator RunTurns(BattleAction playerAction)
    {
        state = BattleState.RunningTurn;

        if(playerAction == BattleAction.Move)
        {
            playerUnit.Chimere.CurrentMove = playerUnit.Chimere.Moves[currentMove];
            ennemyUnit.Chimere.CurrentMove = ennemyUnit.Chimere.GetRandomMove();

            int playerMovePriority = playerUnit.Chimere.CurrentMove.Base.Priority;
            int ennemyMovePriority = ennemyUnit.Chimere.CurrentMove.Base.Priority;

            // Check who goes first
            bool playerGoesFirst = true;
            if (ennemyMovePriority > playerMovePriority)
                playerGoesFirst = false;
            else if(ennemyMovePriority == playerMovePriority)
                playerGoesFirst = playerUnit.Chimere.Speed >= ennemyUnit.Chimere.Speed;

            var firstUnit = (playerGoesFirst) ? playerUnit : ennemyUnit;
            var secondUnit = (playerGoesFirst) ? ennemyUnit : playerUnit;

            var secondChimere = secondUnit.Chimere;

            // First Turn
            yield return RunMove(firstUnit, secondUnit, firstUnit.Chimere.CurrentMove);
            yield return RunAfterTurn(firstUnit);
            if (state == BattleState.BattleOver) yield break;

            if (secondChimere.HP > 0)
            {
                // Second Turn
                yield return RunMove(secondUnit, firstUnit, secondUnit.Chimere.CurrentMove);
                yield return RunAfterTurn(secondUnit);
                if (state == BattleState.BattleOver) yield break;
            }
        }
        //Switch chimere
        else
        {
            if(playerAction == BattleAction.SwitchChimere)
            {
                var selectedChimere = playerParty.Chimeres[currentMember];
                state = BattleState.Busy;
                yield return SwitchChimere(selectedChimere);
            }
            //Ennemy turn after switch chimere
            var ennemyMove = ennemyUnit.Chimere.GetRandomMove();

            yield return RunMove(ennemyUnit, playerUnit, ennemyMove);
            yield return RunAfterTurn(ennemyUnit);
            if (state == BattleState.BattleOver) yield break;
        }

        if (state != BattleState.BattleOver)
            ActionsSelection();
    }

    IEnumerator RunMove(BattleUnit sourceUnit, BattleUnit targetUnit, Move move)
    {
        bool canRunMove = sourceUnit.Chimere.OnBeforeMove();
        if (!canRunMove)
        {
            yield return ShowStatusChanges(sourceUnit.Chimere);
            yield return sourceUnit.Hud.UpdateHP();
            yield break;
        }
        yield return ShowStatusChanges(sourceUnit.Chimere);

        move.PP--;
        yield return dialogBox.TypeDialog($"{ sourceUnit.Chimere.Base.Name} used {move.Base.Name}");

        // vérifie que l'attaque à une chance rater grace au bool "alwaysHit" dans moveBase
        if (CheckIfMoveHits(move, sourceUnit.Chimere, targetUnit.Chimere))
        {
            if (move.Base.Category == MoveCategory.Status)
            {
                yield return RunMoveEffects(move.Base.Effects, sourceUnit.Chimere, targetUnit.Chimere, move.Base.Target);
            }
            else
            {
                var damageDetails = targetUnit.Chimere.TakeDamage(move, sourceUnit.Chimere);
                yield return targetUnit.Hud.UpdateHP();
                yield return ShowDamageDetails(damageDetails);
            }
            if(move.Base.SecondaryEffects != null && move.Base.SecondaryEffects.Count >0 && targetUnit.Chimere.HP > 0)
            {
                foreach (var secondary in move.Base.SecondaryEffects)
                {
                    yield return RunMoveEffects(secondary, sourceUnit.Chimere, targetUnit.Chimere, secondary.Target);
                }
            }

            if (targetUnit.Chimere.HP <= 0)
            {
                yield return dialogBox.TypeDialog($"{targetUnit.Chimere.Base.Name} Fainted");
                // playFaintedAnimation
                yield return new WaitForSeconds(2f);

                CheckForBattleOver(targetUnit);
            }
        }
        //  l'attaque a échoué
        else
        {
            yield return dialogBox.TypeDialog($"{sourceUnit.Chimere.Base.Name}'s attack missed");
        }

    }
    IEnumerator RunMoveEffects(MoveEffects effets,Chimere source, Chimere target, MoveTarget moveTarget)
    {
        //Stat boosting
        if (effets.Boosts != null)
        {
            if (moveTarget == MoveTarget.Self)
                source.ApplyBoosts(effets.Boosts);
            else
                target.ApplyBoosts(effets.Boosts);
        }
        //Status Condition
        if(effets.Status != ConditionID.none)
        {
            target.SetStatus(effets.Status);
        }
        //volatile Status Condition
        if (effets.VolatileStatus != ConditionID.none)
        {
            target.SetVolatileStatus(effets.VolatileStatus);
        }

        yield return ShowStatusChanges(source);
        yield return ShowStatusChanges(target);
    }
    IEnumerator RunAfterTurn(BattleUnit sourceUnit)
    {
        if (state == BattleState.BattleOver) yield break;
        yield return new WaitUntil(() => state == BattleState.RunningTurn);

        // statuts blesse la chimere after chaque tour
        sourceUnit.Chimere.OnAfterTurn();
        yield return ShowStatusChanges(sourceUnit.Chimere);
        yield return sourceUnit.Hud.UpdateHP();

        if (sourceUnit.Chimere.HP <= 0)
        {
            yield return dialogBox.TypeDialog($"{sourceUnit.Chimere.Base.Name} Fainted");
            // playFaintedAnimation
            yield return new WaitForSeconds(2f);

            CheckForBattleOver(sourceUnit);
        }
    }

    //Précision
    bool CheckIfMoveHits(Move move, Chimere source, Chimere chimere)
    {

        if (move.Base.AlwaysHit)
            return true;

        float moveAccuracy = move.Base.Accuracy;
        int accuracy = source.StatBoosts[Stat.Accuracy];
        int evasion = source.StatBoosts[Stat.Evasion];

        var boostValues = new float[] { 1f, 4f / 3f, 5f / 3f, 2f, 7f / 3f, 8f / 3f, 3f };

        if (accuracy > 0)
            moveAccuracy *= boostValues[accuracy];
        else
            moveAccuracy /= boostValues[-accuracy];

        if ( evasion> 0)
            moveAccuracy /= boostValues[evasion];
        else
            moveAccuracy *= boostValues[-evasion];

        return UnityEngine.Random.Range(1, 101) <= moveAccuracy;
    }
    IEnumerator ShowStatusChanges(Chimere chimere)
    {
        while (chimere.StatusChanges.Count > 0)
        {
            var message = chimere.StatusChanges.Dequeue();
            yield return dialogBox.TypeDialog(message);
        }
    }

    void CheckForBattleOver(BattleUnit faintedUnit)
    {
        // si la chimere du joueur est vaincu
        if (faintedUnit.IsPlayerUnit)
        {
            //vérifeie si le joueur à d'autre chimere non K.O
            var nextChimere = playerParty.GetHealthyChimere();
            if (nextChimere != null)
                OpenPartyScreen();
            else
                BattleOver(false);
        }
        else
            BattleOver(true);
    }

    IEnumerator ShowDamageDetails(DamageDetails damageDetails)
    {
        if (damageDetails.Critical > 1f)
            yield return dialogBox.TypeDialog("A crtitcal hit !");
        if (damageDetails.TypeEffectivenes > 1f)
            yield return dialogBox.TypeDialog("It's super effective !");
        else if(damageDetails.TypeEffectivenes <1f)
            yield return dialogBox.TypeDialog("It's not very effective ...");
    }
    public void HandleUpdate()
    {
        // début de combat, choix entre fight et run
        if(state == BattleState.ActionSelection)
        {
            HandleActionSelection();
        }
        // tour du joueur, choix de l'attaque
        else if(state == BattleState.MoveSelection)
        {
            HandleMoveSelection();
        }
        // change de chimere
        else if (state == BattleState.PartyScreen)
        {
            HandlePartySelection();
        }

    }
    void HandleActionSelection()
    {
        // choix au début de chaque tour
        if (Input.GetKeyDown(KeyCode.RightArrow))
            ++currentAction;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            --currentAction;
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            currentAction += 2;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            currentAction -= 2;

        currentAction = Mathf.Clamp(currentAction, 0, 3);

            dialogBox.UpdateActionSelection(currentAction);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (currentAction == 0)
            {
                //Fight
                MoveSelection();
            }
            else if (currentAction == 1)
            {
                //Bag
            }
            else if (currentAction == 2)
            {
                //Switch Chimere
                prevStat = state;
                OpenPartyScreen();
            }
            else if (currentAction == 3)
            {
                //Run
            }
        }
    }

    void HandleMoveSelection()
    {
        // passer d'une action à une autre (choix des attaques)
        if (Input.GetKeyDown(KeyCode.DownArrow))
            ++currentMove;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            --currentMove;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            currentMove -= 2;
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            currentMove += 2;

        currentMove = Mathf.Clamp(currentMove, 0, playerUnit.Chimere.Moves.Count -1);

        dialogBox.UpdateMoveSelection(currentMove, playerUnit.Chimere.Moves[currentMove]);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            var move = playerUnit.Chimere.Moves[currentMove];
            if (move.PP == 0) return;

            dialogBox.EnableMoveSelector(false);
            dialogBox.EnableDialogText(true);
            StartCoroutine(RunTurns(BattleAction.Move));
        }
        //retour choix précédent
        else if (Input.GetKeyDown(KeyCode.X))
        {
            dialogBox.EnableMoveSelector(false);
            dialogBox.EnableDialogText(true);
            ActionsSelection();
        }
    }
    void HandlePartySelection()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
            ++currentMember;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            --currentMember;
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            currentMember -= 2;
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            currentMember += 2;

        currentMember = Mathf.Clamp(currentMember, 0, playerParty.Chimeres.Count - 1);

        partyScreen.UpdateMemberSelection(currentMember);

        if (Input.GetKeyDown(KeyCode.Z))
        {
            var selectedMember = playerParty.Chimeres[currentMember];

            //changer de chimere en combat
            if (selectedMember.HP <= 0)
            {
                partyScreen.SetMessageText("you can't send a fainted chimere");
                return;
            }
            if (selectedMember == playerUnit.Chimere)
            {
                partyScreen.SetMessageText("you can't switch with the same chimere");
                return;
            }
            partyScreen.gameObject.SetActive(false);

            if(prevStat == BattleState.ActionSelection)
            {
                prevStat = null;
                StartCoroutine(RunTurns(BattleAction.SwitchChimere));
            }
            else
            {
                state = BattleState.Busy;
                StartCoroutine(SwitchChimere(selectedMember));
            }
        }
        else if (Input.GetKeyDown(KeyCode.X))
        {
            ActionsSelection();
        }
    }

    IEnumerator SwitchChimere (Chimere newchimere)
    {
        if(playerUnit.Chimere.HP > 0)
        {
            yield return dialogBox.TypeDialog($"Come back {playerUnit.Chimere.Base.Name}");
            //playerUnit.PlaySwitchAnimation();
            yield return new WaitForSeconds(2f);
        }

        playerUnit.Setup(newchimere);
        dialogBox.SetMoveNames(newchimere.Moves);
        yield return dialogBox.TypeDialog($"Go {newchimere.Base.Name}");

        state = BattleState.RunningTurn;
    }
}
