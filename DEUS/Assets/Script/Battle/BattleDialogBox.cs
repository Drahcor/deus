﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleDialogBox : MonoBehaviour
{
    [SerializeField] int letterPerSecond;
    [SerializeField] Color highlightedColor;

    [SerializeField] Text dialogText;
    [SerializeField] GameObject actionSelector;
    [SerializeField] GameObject moveSelector;
    [SerializeField] GameObject moveDetails;

    [SerializeField] List<Text> actionsText;
    [SerializeField] List<Text> moveText;

    [SerializeField] Text ppText;
    [SerializeField] Text typeText;

    public void SetDialog(string dialog)
    {
        dialogText.text = dialog;
    }

    internal void SetDialog()
    {
        throw new NotImplementedException();
    }
    public IEnumerator TypeDialog(string dialog)
    {
        dialogText.text = "";

        //vitesse de défilement du txt en lettre par secondes
        foreach (var letter in dialog.ToCharArray())
        {
            dialogText.text += letter;
            yield return new WaitForSeconds(1f/letterPerSecond);
        }
        yield return new WaitForSeconds(1f);
    }
    public void EnableDialogText(bool enabled)
    {
        dialogText.enabled = enabled;
    }
    public void EnableActionSelector(bool enabled)
    {
        actionSelector.SetActive(enabled);
    }
    public void EnableMoveSelector(bool enabled)
    {
        moveSelector.SetActive(enabled);
        moveDetails.SetActive(enabled);
    }
    // change de couleur l'action sélectionné en début de combat
    public void UpdateActionSelection(int selectedAction)
    {
        for (int i=0; i<actionsText.Count; ++i)
        {
            if (i == selectedAction)
                actionsText[i].color = highlightedColor;
            else
                actionsText[i].color = Color.black;
        }
    }
    // change de couleur l'action sélectionné en combat
    public void UpdateMoveSelection(int selectedMove, Move move)
    {
        for(int i=0; i<moveText.Count; i++)
        {
            if (i == selectedMove)
                moveText[i].color = highlightedColor;
            else
                moveText[i].color = Color.black;
        }
        ppText.text = $"PP{move.PP}/{move.Base.PP}";
        typeText.text = move.Base.Type.ToString();

        if(move.PP == 0)
        {
            ppText.color = Color.red;
        }
        else
        {
            ppText.color = Color.black;
        }
    }
    // attaques des Chimères qui apparaissent lors du combat
    public void SetMoveNames(List<Move> moves)
    {
        for (int i=0; i< moveText.Count; i++)
        {
            if (i < moves.Count)
                moveText[i].text = moves[i].Base.Name;
            else
                moveText[i].text = "-";
        }
    }
}
