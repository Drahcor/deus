﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Food Object", menuName = "Inventory System/Items/Fod")]
public class FoodObject : ItemObject
{
    void Awake()
    {
        type = ItemType.Food;
    }
}
