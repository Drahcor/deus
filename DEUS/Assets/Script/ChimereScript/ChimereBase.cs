﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static MoveBase;

[CreateAssetMenu(fileName = "Chimere", menuName = "Chimere/Create new chimere")]

public class ChimereBase : ScriptableObject {

    [SerializeField] string name;

    [TextArea]
    [SerializeField] string description;

    [SerializeField] GameObject chimere3D;

    [SerializeField] ChimereType type1;
    [SerializeField] ChimereType type2;

    // base stats
    [SerializeField] int maxHP;
    [SerializeField] int attack;
    [SerializeField] int defence;
    [SerializeField] int speAttack;
    [SerializeField] int speDefence;
    [SerializeField] int speed;
    [SerializeField] int id;

    [SerializeField] List<LearnableMove> learnableMoves;

    public string Getname()
    {
        return name;
    }
    public string Name
    {
        get { return name; }
    }
    public string Description
    {
        get { return description; }
    }
    public GameObject Chimere3D
    {
        get { return chimere3D; }
    }
    public ChimereType Type1
    {
        get { return type1; }
    }
    public ChimereType Type2
    {
        get { return type2; }
    }
    public int MaxHP
    {
        get { return maxHP; }
    }
    public int Attack
    {
        get { return attack; }
    }
    public int Defence
    {
        get { return defence; }
    }
    public int SpeAttack
    {
        get { return speAttack; }
    }
    public int SpeDefence
    {
        get { return speDefence; }
    }
    public int Speed
    {
        get { return speed; }
    }
    public int ID
    {
        set { Random.Range(0,9999); }
        get { return id; }
    }

    public List<LearnableMove> LearnableMoves
    {
        get { return learnableMoves; }
    }
}
[System.Serializable]
public class LearnableMove
{
    [SerializeField] MoveBase moveBase;
    [SerializeField] int level;

    public MoveBase Base
    {
        get { return moveBase; }
    }

    public int Level
    {
        get { return level; }
    }
}

