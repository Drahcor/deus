﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChimereParty : MonoBehaviour
{
    [SerializeField] List<Chimere> chimeres;

    public List<Chimere> Chimeres
    {
        get
        {
            return chimeres;
        }
    }

    private void Start()
    {
        foreach(var chimere in chimeres)
        {
            chimere.Init();
        }
    }
    public Chimere GetHealthyChimere()
    {
       return chimeres.Where(x => x.HP > 0).FirstOrDefault();
    }
}