﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Move", menuName = "Chimere/Create new move")]

public class MoveBase : ScriptableObject {

    [SerializeField] string name;

    [TextArea]
    [SerializeField] string description;

    [SerializeField] public ChimereType Type;
    [SerializeField] int power;
    [SerializeField] int accuracy;
    [SerializeField] bool alwaysHit;
    [SerializeField] int pp;
    [SerializeField] int priority;
    [SerializeField] MoveCategory category;
    [SerializeField] MoveEffects effects;
    [SerializeField] List<SecondaryEffects> secondaryEffects;
    [SerializeField] MoveTarget target;

    public string Name
    {
        get { return name; }
    }
    public string Description
    {
        get { return description; }
    }
    public ChimereType type
    {
        get { return Type; }
    }
    public int Power
    {
        get { return power; }
    }
    public int Accuracy
    {
        get { return accuracy; }
    }
    public bool AlwaysHit
    {
        get { return alwaysHit; }
    }
    public int PP
    {
        get { return pp; }
    }
    public int Priority
    {
        get { return priority; }
    }
    public MoveCategory Category
    {
        get { return category; }
    }
    public MoveEffects Effects
    {
        get { return effects; }
    }
    public List<SecondaryEffects> SecondaryEffects
    {
        get { return secondaryEffects; }
    }
    public MoveTarget Target
    {
        get { return target; }
    }
}

public enum ChimereType
{
    None,
    Basic,
    Perforant,
    Blindé,
    Tranchant
}
public enum Stat
{
    Attack,
    Defence,
    SpAttack,
    SpDefence,
    Speed,

    Accuracy,
    Evasion
}

// tableau des types
public class TypeChart
{
    static float[][] chart =
    { 
              //ATTAQUE ----->
        //                          basic   perfo    tranc      blind     
        /* basic*/      new float [] {1f,      1f,    1f,        1f},
        /* perforant*/  new float [] {1f     ,0.5f,   2f,      0.5f},         //DEFENCE
        /* tranchant*/  new float [] {1f     ,0.5f,  0.5f,       2f},        
        /* blindé*/     new float [] {1f      ,2f,   0.5f,     0.5f}
    };
    public static float GetEffictiveness (ChimereType attackType, ChimereType defenceType)
    {
        if (attackType == ChimereType.None || defenceType == ChimereType.None)
            return 1;

        int row = (int)attackType - 1;
        int col = (int)defenceType - 1;

        return chart[row][col];
    }
}
[System.Serializable]
public class MoveEffects
{
    [SerializeField] List<StatBoost> boosts;
    [SerializeField] ConditionID status;
    [SerializeField] ConditionID volatileStatus;

    public List<StatBoost> Boosts
    {
        get { return boosts; }
    }
    public ConditionID Status
    {
        get { return status; }
    }
    public ConditionID VolatileStatus
    {
        get { return volatileStatus; }
    }
}

[System.Serializable]
public class SecondaryEffects : MoveEffects
{
    [SerializeField] MoveTarget target;

    public MoveTarget Target
    {
        get { return target; }
    }

}

[System.Serializable]
public class StatBoost
{
    public Stat stat;
    public int boost;
}

public enum MoveCategory
{
    Physical, Special, Status
}

public enum MoveTarget
{
    Foe, Self
}

