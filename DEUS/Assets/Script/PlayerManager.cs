﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {

    public MouseItem mouseItem = new MouseItem();

    public CharacterController controller;
    public Transform cam;

    //marche
    public float speed = 6.00f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    public bool isMoving;

    //jump
    public bool isGrounded;
    public float gravity = -9.81f;
    public float jumpHeight = 3;
    Vector3 velocity;


    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    // scene combat
    public event Action OnEncounter;

    //Inventory
    public InventoryObject inventory;

    private void Start()
    {
        isMoving = true;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            inventory.Save();
            Debug.Log("inventory save");
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            inventory.Load();
            Debug.Log("inventory load");
        }
    }
    public void HandleUpdate()
    {
        if (isMoving)
        {
            // Déplacement (marche)
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

            //jump
            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -1f;
            }

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * velocity.y * gravity);
            }
            //gravity
            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);

            //rotation de la caméra autour du joueur
            if (direction.magnitude >= 0.1f)
            {
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward; // permet au joueur de tourner dans le même sens que la caméra
                controller.Move(moveDir.normalized * speed * Time.deltaTime);
            }
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // le joueur touche un ennmis
        if (hit.gameObject.tag == "ennemis")
        {
            OnEncounter();
        }

        var item = hit.transform.GetComponent<GroundItem>();
        if (item)
        {
            inventory.AddItem(new Item(item.item), 1);
            Destroy(hit.gameObject);
        }
    }

    private void OnApplicationQuit()
    {
        inventory.Container.Items = new InventorySlot[24];
    }
}